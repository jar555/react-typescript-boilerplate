import React, {Component} from 'react';
import './landing.scss';
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';
import {SignIn} from './sign-in/sign-in';
import {SignUp} from './sign-up/sign-up';

export class Landing extends Component {
    render(): React.ReactNode {
        return (
            <Router>
                {/*<nav>
                    <ul>
                        <li>
                            <Link to="/sign-up">Zarejestruj</Link>
                        </li>
                        <li>
                            <Link to="/">Zaloguj</Link>
                        </li>
                    </ul>
                </nav>*/}
                <div id="landing">
                    <Route path="/" exact component={SignIn}/>
                    <Route path="/sign-up" component={SignUp}/>
                </div>
            </Router>
        );
    }
}
