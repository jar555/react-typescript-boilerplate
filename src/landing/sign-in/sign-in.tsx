import React, {Component} from 'react';
import './sign-in.scss';
import {Input} from '../../shared/form/input/input';

export class SignIn extends Component {
    signIn(): void {
        console.log('sigm in');
    }

    render(): React.ReactNode {

        return (
            <div id="sign-in">
                <div className="container">
                    <div id="sign-in-form">
                        <header>Sign In</header>
                        <form>
                            <Input/>
                        </form>
                        <footer>
                            <button onClick={this.signIn}>Sign in!</button>
                        </footer>

                    </div>
                </div>
            </div>
        );
    }
}
