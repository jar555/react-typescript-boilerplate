import React, {Component} from 'react';
import './button.scss';

export class Button extends Component {
    render(): React.ReactNode {
        return (
            <button>
                Button
            </button>
        );
    }
}
