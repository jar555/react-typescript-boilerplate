import React, {Component} from 'react';

export interface InputProps {
    type?: string;
}

export class Input extends Component {
    constructor(props: InputProps) {
        super(props);
    }



    render(): React.ReactNode {
        return (
            <input type={'text'}/>
        );
    }
}
