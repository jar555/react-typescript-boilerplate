import React from 'react';
import './App.scss';
import {Dashboard} from './dashboard/dashboard';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import {Landing} from './landing/landing';

const App: React.FC = () => {
    return (
        <Router>
            <div className="app">
                <Route path="/" component={Landing} />
                <Route path="/dashboard/" component={Dashboard} />
            </div>
        </Router>
    );
};

export default App;
